#!/bin/bash

SCRIPT_DIR="${0%/*}"
GIT_URL="$1"
PROJECT="${GIT_URL##*/}"

cd $SCRIPT_DIR
mkdir -p cache

echo "Updating $SCRIPT_DIR/cache/$PROJECT ($GIT_URL)"
if [ ! -d cache/$PROJECT ]; then
	git clone $GIT_URL cache/$PROJECT
	cd cache/$PROJECT
	range=""
else
	cd cache/$PROJECT
	range="$(git log -1 --format=%H).."
	git pull
	
fi

commits=$(git log --format=%h --reverse $range | wc -l)
echo "Generating mbox ($commits new commits found)"
for h in $(git log --format=%h --reverse $range); do git show $h:m >> mbox; echo >> mbox; done
echo "Done"

