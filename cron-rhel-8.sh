#!/bin/bash

PROJECT="rhel-8"
cd ${0%/*}
export PATH=~/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

./gl2pi.py --config $PROJECT.cfg -v > cache/log-$PROJECT.txt 2>&1
ret=$?

if [ $ret -ne 0 ]; then
	cat cache/log-$PROJECT.txt
fi
cat cache/log-$PROJECT.txt >> cache/log-$PROJECT-all.txt

exit $ret
