#!/usr/bin/env python3

import gitlab

import asyncore
import configparser
import email
import os
import urllib3
from logging import getLogger, DEBUG, INFO, WARN, StreamHandler, Formatter
from optparse import OptionParser
from smtpd import SMTPServer
from smtpd import DebuggingServer
from utils import setup_logging, log_exception, run_command, safe_run_command

log = None
config = None
options = None

def load_config(config_path):
    if not config_path:
        config_path = os.path.join(script_dir, 'e2gl.cfg')
    config = configparser.ConfigParser()
    if not config.read(config_path):
        sys.stderr.write('Failed to parse %s\n' % (config_path))
        sys.exit(1)
    log.debug('Config loaded: %s', config_path)

    return config

class MyServer(SMTPServer):
    no = 0
    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        log.debug('peer: %s', peer)
        log.debug('mailfrom: %s', mailfrom)
        log.debug('rcpttos: %s', rcpttos)
        log.debug('data: %s', data)
        log.debug('kwargs: %s', kwargs)
        msg = email.message_from_bytes(data)

        to_addr = rcpttos[0]
        body = msg.get_payload()

        return send_reply_to_gitlab(to_addr, body)


    def smtp_EHLO(self, arg):
        print(arg)
        if not arg:
            self.push('501 Syntax: HELO hostname')
            return
        if self.__greeting:
            self.push('503 Duplicate HELO/EHLO')
        else:
            self.__greeting = arg
            self.push('250 %s' % self.__fqdn)

def run_server():
    foo = MyServer(('localhost', 9025), None)
    try:
        log.debug('Starting to listen on port: %s', 9025)
        asyncore.loop()
    except KeyboardInterrupt:
        pass

def parse_args(parser):
    parser.add_option('--config', dest='config', help='config file', default='e2gl.cfg')
    parser.add_option('-v', '--verbose', dest='verbose',
        action='count', default=0, help='verbosity level')

    return parser.parse_args()

def init(options):
    global config
    urllib3.disable_warnings()

    if options.verbose > 0:
        log.setLevel(DEBUG)
        log.debug('Debug logging on')

    config = load_config(options.config)
    config.options = options

    log.debug('Initializing gitlab connection')
    gitlab_api_token = config['main']['GITLAB_API_TOKEN']

    config.lab = gitlab.Gitlab('https://%s' % (config['main']['GITLAB_HOST']),
        private_token=gitlab_api_token, ssl_verify=False)

    log.debug('Init done')

def send_reply_to_gitlab(rcpt, body):
    if not rcpt:
        return '455 bad recipient address format: NULL'

    pos = rcpt.find('@g')
    if pos == -1:
        return '455 bad recipient address format: %s' % (rcpt)

    rcpt = rcpt[pos+2:]
    log.debug('rcpt: %s', rcpt)
    fields = rcpt.split('.')

    stype = fields[0]
    sproject = fields[1]
    mr_iid = fields[2]

    try:
        project = config.lab.projects.get(sproject)
        mr_obj = project.mergerequests.get(mr_iid)

        if stype in ['t', 'd', 'c']:
            mr_note = mr_obj.notes.create({'body': body})
        elif stype in ['n']:
            disc_id = fields[3]
            disc_obj = mr_obj.discussions.get(disc_id)
            mr_note = disc_obj.notes.create({'body': body})
        log.debug('note created: %s', mr_note.id)
    except Exception as e:
        return '455 %s' % (str(e))



def main():
    global options
    global log
    log = setup_logging()

    parser = OptionParser(usage='Usage: %prog [options]')
    (options, args) = parse_args(parser)

    init(options)
    run_server()

    log.debug('Exiting.')

if __name__ == '__main__':
    main()

