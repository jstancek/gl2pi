#!/usr/bin/env python3

import gitlab

import email
import hashlib
import json
import re
import os
import shlex
import sys
import urllib3
from datetime import datetime, timedelta
from email.message import EmailMessage
from logging import getLogger, DEBUG, INFO, WARN, StreamHandler, Formatter
from optparse import OptionParser

from utils import setup_logging, log_exception, load_config, run_command, safe_run_command

log = None
config = None
options = None

def parse_args(parser):
    parser.add_option('--config', dest='config', help='config file', default=None)
    parser.add_option('-v', '--verbose', dest='verbose',
        action='count', default=0, help='verbosity level')
    parser.add_option('-r', '--refetch-all', action='store_true', dest='refetch_all',
        default = False, help='refetch all MRs')
    parser.add_option('-l', '--limit', dest='limit', action='store_true',
        default = False, help='limit MRs to process X records at a time (for testing)')

    return parser.parse_args()

def init(options):
    global config
    urllib3.disable_warnings()

    if options.verbose > 0:
        log.setLevel(DEBUG)
        log.debug('Debug logging on')

    config = load_config(options.config)
    config.options = options

    log.debug('Initializing gitlab connection')
    f = open(os.path.expanduser(config['main']['GITLAB_API_TOKEN']))
    gitlab_api_token = f.read().strip()
    f.close()

    config.lab = gitlab.Gitlab('https://%s' % (config['main']['GITLAB_HOST']),
        private_token=gitlab_api_token, ssl_verify=False)
    config.project = config.lab.projects.get(config.project_number)

    os.makedirs(config.cachedir, exist_ok = True)
    if not os.path.exists(config.public_inbox_dir):
        log.debug('Creating %s', config.public_inbox_dir)
        os.makedirs(config.public_inbox_dir, exist_ok = True)
        safe_run_command('git init', cwddir=config.public_inbox_dir)

    log.debug('Init done')

def git_fetch_updates():
    if not os.path.exists(config.gitrepo):
        safe_run_command('git clone --mirror %s %s' % (config['main']['GITLAB_REPO_URL'], 'repo'),
            cwddir=config.cachedir)
    else:
        safe_run_command('git fetch --all', cwddir=config.gitrepo)
    safe_run_command('git fetch origin +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*',
        cwddir=config.gitrepo)

def get_mr_cache_dir(mr_obj):
    return os.path.join(config.cachedir, str(mr_obj.iid))

def save_last_mr_state(mr_obj, cached_state):
    mr_cache_dir = get_mr_cache_dir(mr_obj)
    if not os.path.exists(mr_cache_dir):
        os.makedirs(mr_cache_dir)
    mr_cache_file = os.path.join(mr_cache_dir, 'data.json')
    with open(mr_cache_file, 'w') as f:
        json.dump(cached_state, f, indent=4)

def load_last_mr_state(mr_obj):
    last_state = {}

    mr_cache_dir = get_mr_cache_dir(mr_obj)
    if not os.path.exists(mr_cache_dir):
        os.makedirs(mr_cache_dir)

    mr_cache_file = os.path.join(mr_cache_dir, 'data.json')
    if os.path.exists(mr_cache_file):
        with open(mr_cache_file) as f:
            last_state = json.load(f)
    if not 'iid' in last_state:
        last_state['iid'] = 0
    if not 'Message-ID' in last_state:
        last_state['Message-ID'] = None
    if not 'notes' in last_state:
        last_state['notes'] = {}
    if not 'updated_at' in last_state:
        last_state['updated_at'] = 0
    if not 'user_notes_count' in last_state:
        last_state['user_notes_count'] = -1
    if not 'sha' in last_state:
        last_state['sha'] = ''
    return last_state

def isoformat_to_dt(dt_str):
    if not dt_str:
        return datetime.now()
    dt, _, us = dt_str.partition(".")
    dt = datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S")
    return dt

def calc_hash(input):
    if type(input) is str:
        try:
            input = input.encode('raw_unicode_escape')
        except:
            input = input.encode('utf-16')

    return hashlib.sha256(input).hexdigest();

def pi_add_emailmessages(msgs):

    m_file = os.path.join(config.public_inbox_dir, 'm')
    for msg in msgs:
        with open(m_file, 'w') as f:
            f.write('From localhost.localdomain %s\n' % (msg['Message-Timestamp']))
            f.write(msg.as_string())
        safe_run_command('git add m', cwddir=config.public_inbox_dir, verbose=False)
        subject = msg['Subject']
        from_str = msg['From']
        subject = shlex.quote(subject)
        from_str = shlex.quote(from_str)

        # TODO match email Name <email>
        if '<' in from_str and '>' in from_str and '@' in from_str:
            author='--author=%s' % (from_str)
        else:
            author = '--author="%s <user@gitlab>"' % (from_str)

        safe_run_command('git commit -m %s %s' % (subject, author), cwddir=config.public_inbox_dir)

def pi_add_one_commit(mr_obj, m_file_tmp):
    m_file = os.path.join(config.public_inbox_dir, 'm')

    msg = None
    with open(m_file_tmp) as f2:
        try:
            msg = email.message_from_file(f2, policy=email.policy.default)
        except Exception as ex:
            log_exception(ex)
        finally:
            pass


    if not msg:
        return

    # replace From: field
    from_str = msg['From']
    log.debug('from_str orig %s', from_str)
    if '<' in from_str:
        email_pos = from_str.find('<')
    else:
        email_pos = 0

    email_pos = from_str.find('@', email_pos)
    from_str = from_str[:email_pos]
    from_str = from_str + '@gc.%s.%s.gitlab.com>' % (config.project_number, mr_obj.iid)
    del msg['From']
    msg['From'] = from_str
    log.debug('from_str new: %s', from_str)

    subject = msg['Subject']
    log.debug('subject: %s', subject)

    with open(m_file, 'w') as f:
        msg_ctime = isoformat_to_dt(mr_obj.updated_at).ctime()
        f.write('From localhost.localdomain Mon Sep 17 00:00:00 2001\n')
        f.write(msg.as_string())

    subject = shlex.quote(subject)
    author_str = shlex.quote(from_str)

    safe_run_command('git add m', cwddir=config.public_inbox_dir, verbose=False)
    safe_run_command('git commit -m %s --author=%s' % (subject, author_str), cwddir=config.public_inbox_dir)


def pi_add_mbox(mr_obj, mbox_str):
    wrote_lines = 0
    seen_end_of_git_patch = False
    m_file_tmp = os.path.join(get_mr_cache_dir(mr_obj), 'm.tmp')
    f = open(m_file_tmp, 'wb')

    for line in mbox_str.split(b'\n'):
        if line == b'-- ':
            seen_end_of_git_patch = True

        # TODO "From " isn't very reliable as it can appear in message body too
        if line.startswith(b'From ') and wrote_lines and seen_end_of_git_patch:
            wrote_lines = 0
            seen_end_of_git_patch = False
            f.close()

            pi_add_one_commit(mr_obj, m_file_tmp)

            os.remove(m_file_tmp)
            f = open(m_file_tmp, 'wb')

        wrote_lines = wrote_lines + 1
        f.write(line + b'\n')

    f.close()
    if wrote_lines:
        pi_add_one_commit(mr_obj, m_file_tmp)
    os.remove(m_file_tmp)


def msgs_to_mbox(msgs):
    with open('mbox', 'a') as f:
        for msg in msgs:
            f.write('From localhost.localdomain %s\n' % (msg['Message-Timestamp']))
            f.write(msg.as_string())
            f.write('\n')

def add_to_mbox(something):
    with open('mbox', 'ab') as f:
        f.write(something)

def process_mr_top(mr_obj, cached_state):
    msgs = []
    cached_state['iid'] = mr_obj.iid
    cached_state['updated_at'] = mr_obj.updated_at
    if not cached_state['Message-ID'] or config.options.refetch_all:
        message_id = '<%s-%s-%s@gl2pi>' % (config.project_number, mr_obj.iid, mr_obj.id)
        cached_state['Message-ID'] = message_id

        msg_ctime = isoformat_to_dt(mr_obj.updated_at).ctime()
        msg = EmailMessage()
        msg['From'] = '%s <%s@gt.%s.%s.%s.gitlab.com>' % (mr_obj.author['name'], mr_obj.author['username'],
            config.project_number, mr_obj.iid, mr_obj.id)
        msg['Message-Timestamp'] = msg_ctime
        msg['To'] = 'gl2pi <jstancek@redhat.com>'
        msg['Message-ID'] = message_id
        msg['Subject'] = 'MR !%s %s' % (mr_obj.iid, config.project_name)
        msg.set_content('')
        msgs.append(msg)
        log.debug('Created top msg: %s', message_id)
    return msgs

def process_mr_desc(mr_obj, cached_state):
    msgs = []
    cached_hash = cached_state.get('desc_hash')

    current_desc = '%s%s' % (mr_obj.title, mr_obj.description)
    desc_hash = calc_hash(current_desc)

    if not config.options.refetch_all:
        if desc_hash == cached_hash:
            return msgs

    cached_state['desc_hash'] = desc_hash
    log.debug('Description or title changed')
    timestamp = int(isoformat_to_dt(mr_obj.updated_at).timestamp())
    message_id = '<%s-%s-%s@gl2pi>' % (config.project_number, mr_obj.iid, timestamp)

    msg_ctime = isoformat_to_dt(mr_obj.updated_at).ctime()
    msg = EmailMessage()
    msg['From'] = '%s <%s@gd.%s.%s.gitlab.com>' % (mr_obj.author['name'], mr_obj.author['username'],
        config.project_number, mr_obj.iid)
    msg['Message-Timestamp'] = msg_ctime
    msg['To'] = 'gl2pi <jstancek@redhat.com>'
    msg['Message-ID'] = message_id
    msg['In-Reply-To'] = cached_state['Message-ID']
    msg['References'] = cached_state['Message-ID']
    msg['Subject'] = '[title/description update] %s' % (mr_obj.title)
    msg.set_content(mr_obj.description)
    msgs.append(msg)
    log.debug('Created desc msg: %s', message_id)

    return msgs

def process_mr_commits(mr_obj, cached_state):
    out = ''

    if mr_obj.sha != cached_state['sha'] or config.options.refetch_all:
        cached_state['sha'] = mr_obj.sha

        versions = config.lab.http_list('https://%s/api/v4/projects/%s/merge_requests/%s/versions' %
            (config['main']['GITLAB_HOST'], config['main']['GITLAB_PROJECT'], mr_obj.iid),
            as_list=False)
        num_versions = len(versions)

        base_sha = mr_obj.diff_refs['base_sha'] or ''
        head_sha = mr_obj.diff_refs['head_sha']
        if not head_sha:
            return

        retcode, out, err = safe_run_command(
            ('git rev-list --count  %s..%s') % (base_sha, head_sha),
            abort=False, cwddir=config.gitrepo)
        commits_num = 0
        try:
            commits_num = int(out)
            log.debug('MR %d commits_num: %d', mr_obj.iid, commits_num)
        finally:
            pass
        if commits_num > 5000 or commits_num == 0:
            log.debug('Aborting, because commits_num is %d', commits_num)
            return out

        author_name = shlex.quote(mr_obj.author['name'])
        author_email = shlex.quote(mr_obj.author['username'] + '@gitlab')

        retcode, out, err = safe_run_command(
            ('git -c user.name=%s -c user.email=%s format-patch %s..%s --stdout --in-reply-to="%s" ' +
            '--thread=shallow --cover-letter --encode-email-headers --subject-prefix="PATCH v%s"')
            % (author_name, author_email, base_sha, head_sha, cached_state['Message-ID'], num_versions),
            abort=False, cwddir=config.gitrepo)
        out = out.replace(b'*** SUBJECT HERE ***', str(mr_obj.title).encode('utf_8'), 1)
        out = out.replace(b'*** BLURB HERE ***', str(mr_obj.description).encode('utf_8'), 1)
        #TODO: unique msg-id for all messages
        #out = out.replace(b'Message-Id: <', ('Message-Id: <v%s' % (num_versions)).encode('utf_8'))

        log.debug('Created commits msg: version: %s, range: %s..%s', num_versions, mr_obj.diff_refs['base_sha'], mr_obj.diff_refs['head_sha'])

    return out

def process_mr_notes(mr_obj, cached_state):
    msgs = []

    discussions = mr_obj.discussions.list(all=True)

    for discussion in discussions:
        first_note = None
        for note_dict in discussion.attributes['notes']:
            if not first_note:
                first_note = note_dict

            note_id = str(note_dict['id'])

            note_body = str(note_dict['body'])
            note_hash = calc_hash(note_body)

            if not config.options.refetch_all:
                if note_id in cached_state['notes']:
                    if (note_dict['updated_at'] == cached_state['notes'][note_id]['updated_at']
                        or note_hash == cached_state['notes'][note_id].get('hash')):
                        continue

            if not note_id in cached_state['notes']:
                cached_state['notes'][note_id] = {}
            cached_state['notes'][note_id]['updated_at'] = note_dict['updated_at']
            cached_state['notes'][note_id]['hash'] = note_hash

            # reply to note msg if there is one, otherwise reply to MR msg
            cached_note_msg_id = cached_state['notes'][note_id].get('Message-ID')
            if cached_note_msg_id:
                # this is note update, reply to cache note msg-id
                reply_to_msgid = cached_note_msg_id
            else:
                if note_dict == first_note:
                    # this is first note, reply to cache MR msg-id
                    reply_to_msgid = cached_state['Message-ID']
                else:
                    # this is 2+ note in discussion, reply to msg-id of first note
                    first_note_id = str(first_note['id'])
                    reply_to_msgid = cached_state['notes'][first_note_id]['Message-ID']

            timestamp = int(isoformat_to_dt(note_dict['updated_at']).timestamp())
            message_id = '<%s-%s-%s@gl2pi>' % (config.project_number, note_id, timestamp)
            cached_state['notes'][note_id]['Message-ID'] = message_id

            msg_ctime = isoformat_to_dt(note_dict['updated_at']).ctime()
            msg = EmailMessage()

            author_name = note_dict['author']['name']
            if '@' in note_dict['author']['name']:
                author_name = '"%s"' % (note_dict['author']['name'])

            msg['From'] = '%s <%s@gn.%s.%s.%s.%s.%s.gitlab.com>' % (author_name, note_dict['author']['username'],
                config.project_number, mr_obj.iid, discussion.id, note_id, timestamp)
            msg['Message-Timestamp'] = msg_ctime
            msg['To'] = 'gl2pi <jstancek@redhat.com>'
            msg['Message-ID'] = message_id
            msg['In-Reply-To'] = reply_to_msgid
            msg['References'] = reply_to_msgid
            prefix = ''
            if note_dict['system']:
                prefix = '%s ' % (author_name)
            subject = note_dict['body'].replace('\n', ' ')[:80]
            msg['Subject'] = '%s%s' % (prefix, subject)
            msg.set_content(note_dict['body'])
            msgs.append(msg)
            log.debug('Created note msg, msg_id: %s, From: %s', message_id, msg['From'])

    cached_state['user_notes_count'] = mr_obj.user_notes_count
    return msgs

def process_one_mr(mr_obj):
    log.debug('Processing MR: %s', mr_obj.iid)
    cached_state = load_last_mr_state(mr_obj)

    if (config.options.refetch_all
        or mr_obj.updated_at != cached_state['updated_at']
        or mr_obj.user_notes_count != cached_state['user_notes_count']
        or mr_obj.sha != cached_state['sha']):

        # top MR msg
        msgs = process_mr_top(mr_obj, cached_state)

        # MR description and title
        msgs2 = process_mr_desc(mr_obj, cached_state)

        # MR commits
        commits_in_mbox = process_mr_commits(mr_obj, cached_state)

        # MR notes
        msgs3 = process_mr_notes(mr_obj, cached_state)

        pi_add_emailmessages(msgs + msgs2 + msgs3)
        if commits_in_mbox:
            pi_add_mbox(mr_obj, commits_in_mbox)

        save_last_mr_state(mr_obj, cached_state)


def check_mr_updates(mr_iid = None):
    log.debug('Checking MR updates')
    if mr_iid:
        mr_obj = config.project.mergerequests.get(mr_iid, lazy=False)
        process_one_mr(mr_obj)
        return

    if config.options.refetch_all:
        log.debug('Refetching all')
        with open('mbox', 'w'):
            pass

    # now with some overlap
    dt_now = datetime.today() - timedelta(hours = 2)
    last_update = datetime(1970, 1, 1).isoformat()

    last_state = {}
    state_cache_file = os.path.join(config.cachedir, 'state.json')
    if os.path.exists(state_cache_file) and not config.options.refetch_all:
        with open(state_cache_file) as f:
            last_state = json.load(f)
        if type(last_state) is dict and 'last_update' in last_state:
            last_update = last_state['last_update']
    log.debug('last_update is %s', last_update)

    fetch_all = True
    if options.limit:
        fetch_all = False
    mrs_obj = config.project.mergerequests.list(all=fetch_all,
        updated_after=last_update, lazy=False, view='simple')

    for mr_obj in mrs_obj:
        # re-read with get to get all fields
        mr_obj = config.project.mergerequests.get(mr_obj.iid, lazy=False)
        process_one_mr(mr_obj)

    last_state['last_update'] = dt_now.isoformat()
    with open(state_cache_file, 'w') as f:
        json.dump(last_state, f, indent=4)


def main():
    global options
    global log
    log = setup_logging()

    parser = OptionParser(usage='Usage: %prog [options]')
    (options, args) = parse_args(parser)

    init(options)
    git_fetch_updates()
    check_mr_updates()

    log.debug('Exiting.')

if __name__ == '__main__':
    main()

