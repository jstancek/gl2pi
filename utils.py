import configparser
import os
import sys
import subprocess
import traceback
from logging import getLogger, DEBUG, INFO, WARN, StreamHandler, Formatter

script_dir = os.path.dirname(os.path.realpath(__file__))
log = None

def setup_logging():
    global log
    log_format = '%(asctime)25.25s - %(levelname)9.9s - %(filename)20.20s' \
                    ' :%(lineno)5s %(funcName)20.20s - %(message)s'
    log = getLogger('gl2pi')
    handler = StreamHandler(sys.stderr)
    formatter = Formatter(log_format)
    handler.setFormatter(formatter)
    log.addHandler(handler)
    log.debug('Logging on')
    return log

def log_exception(exc, level=WARN):
    log.log(level, 'type(exc), exc:')
    log.log(level, str(type(exc)))
    log.log(level, str(exc))

    exc_type, exc_value, exc_traceback = sys.exc_info()
    stack = traceback.format_exception(exc_type, exc_value, exc_traceback)

    log.log(level, 'stack:')
    for line in stack:
        log.log(level, line)

def load_config(config_path):
    if not config_path:
        config_path = os.path.join(script_dir, 'default.cfg')
    config = configparser.ConfigParser()
    if not config.read(config_path):
        sys.stderr.write('Failed to parse %s\n' % (config_path))
        sys.exit(1)
    log.debug('Config loaded')

    config.project_url = config['main']['GITLAB_PROJECT_URL']
    config.project_name = config['main']['GITLAB_PROJECT_NAME']
    config.project_remote = config['main']['GITLAB_PROJECT_REMOTE']
    config.project_number = config['main']['GITLAB_PROJECT']
    config.cachedir = os.path.join(script_dir, config['main']['CACHE_DIR'], config.project_number)
    config.gitrepo = os.path.join(config.cachedir, 'repo')
    config.public_inbox_dir = config['main']['PUBLIC_INBOX_REPO']
    return config

def run_command(cmd, cwddir=None):
    p = subprocess.Popen(cmd, cwd=cwddir, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    return p.returncode, out, err

def safe_run_command(cmd, abort=True, cwddir=None, verbose=True):
    if verbose:
        log.debug('[%s] cmd: %s', cwddir, cmd)
    retcode, out, err = run_command(cmd, cwddir)
    if retcode != 0:
        log.warn('[%s] Command failed: %s, ret_code: %d', cwddir, cmd, retcode)
        log.warn(out)
        log.warn(err)
        if abort:
            raise Exception('cmd failed')
    return retcode, out, err


